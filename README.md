# About

Provides migration examples from csv files using drupal 8 migrate, migrate_plus and migrate_source_csv. It adds content types in order to perform the migrations.
The module url: [espurnes/migrate_examples](https://gitlab.com/espurnes/migration_examples).
